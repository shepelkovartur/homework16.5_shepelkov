﻿#include <iostream>
#include <ctime>
const int N = 5;

using namespace std;

int main()
{
	int arr[N][N];

	for (int i = 0; i < N; i++)
	{
		for (int j = 0; j < N; j++)
		{
			arr[i][j] = i + j;
			cout << arr[i][j];
		}
		cout << "\n";
	}

	time_t now = time(0);
	struct tm timeinfo;
	localtime_s(&timeinfo, &now);
	int iday = timeinfo.tm_mday;

	int rem = iday % N;
	int sum = 0;

	for (int i = 0; i < N; i++)
	{
		sum += arr[rem][i];
	}
	cout << sum;
	return 0;
}